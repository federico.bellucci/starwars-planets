import {configure} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

configure({adapter: new Adapter()})

import React from 'react'
import {shallow} from 'enzyme'
import {Planets} from '../../../components/Planets'
import {mockResponse} from '../../../utils/mock'

describe('<Planets /> UI Component', () => {
    it('should render with no errors', () => {
        let wrapper = shallow(<Planets/>)

        // console.log(wrapper.debug())
        return expect(
            wrapper.find('div.planets').length
        ).toBe(1)
    })

})

describe('<Planets /> getFilmUrlSet', () => {
    let filmUrlSet
    beforeAll(() => {
        let wrapper = shallow(<Planets/>)

        filmUrlSet = wrapper.instance().getFilmUrlSet([
            {
                films: [
                    'https://swapi.co/api/films/6/',
                    'https://swapi.co/api/films/1/'
                ],
            },
            {
                films: []
            },
            {
                films: [
                    'https://swapi.co/api/films/6/',
                    'https://swapi.co/api/films/3/'
                ],
            }
        ])
    })

    it('should have 3 unique film urls', () => {
        return expect(
            filmUrlSet.size
        ).toBe(3)
    })

    it('should contain film 1', () => {
        return expect(
            filmUrlSet.has('https://swapi.co/api/films/1/')
        ).toBe(true)
    })

    it('should contain film 3', () => {
        return expect(
            filmUrlSet.has('https://swapi.co/api/films/3/')
        ).toBe(true)
    })

    it('should contain film 6', () => {
        return expect(
            filmUrlSet.has('https://swapi.co/api/films/6/')
        ).toBe(true)
    })

})

describe('<Planets /> loadFilms (should not request films already downloaded)', () => {
    let addFilm,
        requestingFilms = [
            'https://swapi.co/api/films/1/',
            'https://swapi.co/api/films/3/',
            'https://swapi.co/api/films/4/',
            'https://swapi.co/api/films/6/',
        ]

    beforeAll(() => {
        let filmStore = {
            1: {url: 'https://swapi.co/api/films/1/'},
            4: {url: 'https://swapi.co/api/films/4/'}
        }

        addFilm = jest.fn()

        window.fetch =
            jest.fn()
                .mockImplementationOnce((url) => Promise.resolve(mockResponse(200, null, `{"url": "${url}"}`)))
                .mockImplementationOnce((url) => Promise.resolve(mockResponse(200, null, `{"url": "${url}"}`)))


        let wrapper = shallow(<Planets films={filmStore} addFilm={addFilm}/>)

        wrapper.instance().loadFilms(new Set(requestingFilms))
    })

    it('should call addFilm 2 times', () => {
        return expect(
            addFilm.mock.calls.length
        ).toBe(2)
    })

    it('should first call addFilm(3)', () => {
        return expect(
            addFilm.mock.calls[0][0]
        ).toBe("3")
    })

    it('should then call addFilm(6)', () => {
        return expect(
            addFilm.mock.calls[1][0]
        ).toBe("6")
    })
})