import {compare} from '../../utils/index'


describe('compare', () => {

    it('should state that [] < "a" ', () => {
        return expect(
            compare([], 'a')
        ).toBe(-1)
    })

    it('should state that "a" has same order as ["a"] ', () => {
        return expect(
            compare('a', ['a'])
        ).toBe(0)
    })

    it('should state that "a" < ["b"] ', () => {
        return expect(
            compare('a', ['b'])
        ).toBe(-1)
    })

    it('should state that "a" > [] ', () => {
        return expect(
            compare('a', [])
        ).toBe(1)
    })

    it('should state that [] < "a" ', () => {
        return expect(
            compare([], 'a')
        ).toBe(-1)
    })

    it('should state that ["a"] < "b" ', () => {
        return expect(
            compare(['a'], 'b')
        ).toBe(-1)
    })

    it('should state that "b" > ["a"] ', () => {
        return expect(
            compare('b', ['a'])
        ).toBe(1)
    })

    it('should state that ["b"] > "a" ', () => {
        return expect(
            compare(['b'], 'a' )
        ).toBe(1)
    })


    it('should state that "a" < "b"', () => {
        return expect(
            compare('a', 'b')
        ).toBe(-1)
    })

    it('should state that "a" has same order as "a"', () => {
        return expect(
            compare('a', 'a')
        ).toBe(0)
    })

    it('should state that "b" > "a"', () => {
        return expect(
            compare('b', 'a')
        ).toBe(1)
    })

    it('should state that 1 > 0', () => {
        return expect(
            compare(1, 0)
        ).toBe(1)
    })

    it('should state that 1 has same order as 1', () => {
        return expect(
            compare(1, 1)
        ).toBe(0)
    })

    it('should state that "10" < "100"', () => {
        return expect(
            compare('10', '100')
        ).toBe(-1)
    })

    it('should state that 10 < "100"', () => {
        return expect(
            compare(10, '100')
        ).toBe(-1)
    })

    it('should state that "10" < 100', () => {
        return expect(
            compare('10', 100)
        ).toBe(-1)
    })

})
