import {isNumberString} from '../../utils/index'


describe('isNumberString', () => {

    it('should throw Error if param is not string', () => {
        let notStringIsNumber = () => isNumberString(null)
        return expect(notStringIsNumber).toThrowError()
    })

    it('should reject "a"', () => {
        return expect(
            isNumberString('a')
        ).toBe(false)
    })

    it('should accept "1"', () => {
        return expect(
            isNumberString('1')
        ).toBe(true)
    })

    it('should accept "0.1"', () => {
        return expect(
            isNumberString('0.1')
        ).toBe(true)
    })

    it('should reject ".1"', () => {
        return expect(
            isNumberString('.1')
        ).toBe(false)
    })

    it('should accept "-1"', () => {
        return expect(
            isNumberString('-1')
        ).toBe(true)
    })

    it('should accept "-0.1"', () => {
        return expect(
            isNumberString('-0.1')
        ).toBe(true)
    })

    it('should reject "a1"', () => {
        return expect(
            isNumberString('a1')
        ).toBe(false)
    })

    it('should reject "1a"', () => {
        return expect(
            isNumberString('1a')
        ).toBe(false)
    })
})