import React, { Component } from 'react'
import './App.css'
import Planets from './components/Planets/index'

class App extends Component {
    render() {
        return (
            <div className="container">
            	<h1>Starwars Planets</h1>
            	<h3 className="mb-5">Browse the planets that appear in the Star Wars series</h3>
                <Planets />
            </div>
        )
    }
}

export default App
