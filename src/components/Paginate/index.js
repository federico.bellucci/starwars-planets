import React from 'react'
import PropTypes from 'prop-types'
import ReactPaginate from 'react-paginate'

let Paginate = (props) => <ReactPaginate {...props} />

Paginate.propTypes = {
    pageCount: PropTypes.number.isRequired,
    onPageChange: PropTypes.func.isRequired,
    initialPage: PropTypes.number,
    marginPagesDisplayed: PropTypes.number,
    pageRangeDisplayed: PropTypes.number,
    previousLabel:PropTypes.string,
    nextLabel:PropTypes.string,
    breakLabel:PropTypes.node,
    breakClassName:PropTypes.string,
    activeClassName:PropTypes.string,
    containerClassName:PropTypes.string,
    pageClassName:PropTypes.string,
    nextClassName:PropTypes.string,
    previousClassName:PropTypes.string,
    pageLinkClassName:PropTypes.string,
    nextLinkClassName:PropTypes.string,
    previousLinkClassName:PropTypes.string,
}

Paginate.defaultProps = {
    initialPage: 0,
    marginPagesDisplayed: 2,
    pageRangeDisplayed: 5,
    previousLabel: 'previous',
    nextLabel: 'next',
}

export default Paginate
