import React from 'react'
import PropTypes from 'prop-types'
import Paginate from './index'

export const pageLinkClassName = 'page-link',
    linkClassName = 'page-item'

let BootstrapPaginate = (props) => <Paginate {...props} />

BootstrapPaginate.propTypes = {
    ...Paginate.propTypes,
    pageCount: PropTypes.number.isRequired,
    onPageChange: PropTypes.func
}

BootstrapPaginate.defaultProps = {
    breakLabel: <a className="page-link" href="">...</a>,
    breakClassName: 'break-me',
    containerClassName: 'pagination justify-content-center',
    activeClassName: 'active',
    pageClassName: linkClassName,
    nextClassName: linkClassName,
    previousClassName: linkClassName,
    pageLinkClassName: pageLinkClassName,
    nextLinkClassName: pageLinkClassName,
    previousLinkClassName: pageLinkClassName,
}

export default BootstrapPaginate
