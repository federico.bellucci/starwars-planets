import React from 'react'
import { Th, Td } from 'react-super-responsive-table'
import './index.css'
import {compare} from '../../utils/index'
import ResponsiveTable from './index'

export const ORDER_NONE = 0
export const ORDER_ASC  = 1
export const ORDER_DESC = 2

class SortedTable extends ResponsiveTable {

    constructor () {
        super() 

        this.state = {
            sortBy: null,
            sortOrder: null
        }

        this.handleChangeOrder = this.handleChangeOrder.bind(this)
    }

    handleChangeOrder (column) {
        let previousColumn = this.state.sortBy,
            order = this.state.sortOrder
        //console.log('handleChangeOrder', column, previousColumn, order)
        if (previousColumn === column) {
            order = (order + 1) % 3
        } else {
            order = ORDER_ASC
        }
        if (order === 0) {
            column = null
        }
        this.setState({
            sortBy: column,
            sortOrder: order,
        })
    }

    sortData (data, sortBy, sortOrder) {
        //console.log('sortData', sortBy, sortOrder)
        if (sortBy && sortOrder) {
            return Array.from(data).sort((a, b) => {
                a = a[sortBy]
                b = b[sortBy]
                if (sortOrder === ORDER_DESC) {
                    let temp = a
                    a = b
                    b = temp
                }
                return compare(a,b)
            })
        }
        return data
    }

    renderRows (data, adapter, keyGetter) {
        let {sortBy, sortOrder} = this.state
        //console.log('renderRows', data)
        return super.renderRows(
            this.sortData(data, sortBy, sortOrder), 
            adapter, 
            keyGetter
        )
    }

    renderRowItem (key, value) {
        let {sortBy, sortOrder} = this.state,
            className = null
        if (key === sortBy) {
            //console.log('renderRowItems', row.name, key, sortBy, sortOrder)
            if (sortOrder === ORDER_ASC) {
                className = 'sort-pivoted-asc'
            }
            if (sortOrder === ORDER_DESC) {
                className = 'sort-pivoted-desc'
            }
        }
        return (
            <Td className={className} key={key}>
                <div>{value}</div>
            </Td>
        )
    }

    renderHeader(name, label, sortable) {
        return <Th key={name}>
            {sortable 
                ? (
                    <a href="" onClick={e => {e.preventDefault();this.handleChangeOrder(name)}}>
                        <span>{label}</span>{this.renderSortArrow(name)}
                    </a>
                )
                : label
            }
        </Th>
    }

    renderSortArrow(column) {
        if (column === this.state.sortBy) {
            switch (this.state.sortOrder) {
            case ORDER_ASC:
                return <span className="sort-asc">↑</span>
            case ORDER_DESC:
                return <span className="sort-desc">↓</span>
            default:
                return <span className="sort-none"></span>
            }
        }
        return <span className="sort-none"></span>
    }
    
}

SortedTable.propTypes = {
    ...ResponsiveTable.propTypes
}

export default SortedTable
