import React from 'react'
import Film from '../Film'
import TableAdapterBase from '../Table/adapter'

export default class TableAdapter extends TableAdapterBase {

    getHeaders() {
        return {
            name: {
                label: 'Name',
                sortable: true,
            },
            population: {
                label: 'Population',
                sortable: true,
            },
            diameter: {
                label: 'Diameter',
                sortable: true,
            },
            rotation_period: {
                label: 'Rotation Period',
                sortable: true,
            },
            orbital_period: {
                label: 'Orbital Period',
                sortable: true,
            },
            terrain: {
                label: 'Terrain',
            },
            films: {
                label: 'Films',
            },            
        }
    }

    getRows(planets) {
        return planets.map(planet => {
            let {films, ...row} = planet
            return {
                ...row,
                films : this.renderFilms(films)
            }
        })
    }

    renderFilms (films) {
        return <ul>{films.map(url => <Film key={url} url={url}/>)}</ul>
    }

}