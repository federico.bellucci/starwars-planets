import {ADD_FILM} from './actions'

const intialState = {
}

const films = (state = intialState, action) => {
    switch (action.type) {
    case ADD_FILM: 
    	return {
        	...state, 
        	[action.id]: action.film
        }
    default:
        return state
    }
}

export default films