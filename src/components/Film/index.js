import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'

export const getFilmId = (url) => url.match(/https:\/\/swapi\.co\/api\/films\/(\w)\/?/)[1]

class Film extends Component {

    render () {
    	let id = getFilmId(this.props.url)
        if (!this.props.films[id]) {
        	return <li>Loading film…</li>
        }
    	return <li>{this.props.films[id].title}</li>
    }
}

const mapStateToProps = state => {
    return {
        films: state.films
    }
}


Film.propTypes = {
    url: PropTypes.string,
}

export default connect(mapStateToProps)(Film)